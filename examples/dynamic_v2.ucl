module main {

	const N : integer =3;
	
	type enum_state = enum {PROCEED,STALL, ROLLBACK};

	type t_cmd = record {
		selslow : boolean,
		commit : boolean,
		nextin : boolean,
		invalid : boolean,
		valid : boolean,
		masked : boolean,
		rollback : integer,
		cpt : integer
		
	};
	type t_state = record {
		state : enum_state,
		cpt : integer
		
	};

	type array_int = [integer]  integer;

	var seq_i : integer;
	var seq_x : integer;
	var pipe_i : integer;
	var pipe_x : integer;

	var seq_stream_x : array_int;
	var pipe_stream_x : array_int;

	define update_x(x : integer) : integer =
		if (cond(x)) then	
			slow(x) 
		 else 
			fast(x);
			
	function cond(in_0 : integer) : boolean;
	function slow(in_0 : integer) : integer;
	function fast(in_0 : integer) : integer;
	
	procedure init_seq(xin:integer) modifies seq_i,seq_x,seq_stream_x; 
	{
		seq_i=0;
		seq_x=xin;
		seq_stream_x[0]=xin;
	}
	  
	procedure next_seq() modifies seq_i,seq_x,seq_stream_x; 
	{
		seq_i=(seq_i+1); 
		seq_x = update_x(seq_x);
		seq_stream_x[seq_i]=seq_x;
	}


	var cs : enum_state;
	var cpt : integer;
 	var slow_stage0,slow_stage1,slow_stage2,slow_stage3 : integer;

	procedure init_pipe(xin:integer) modifies pipe_i,pipe_x,
		slow_stage0,slow_stage1,slow_stage2,slow_stage3,
		pipe_stream_x; 
	{
		pipe_i=0;
		pipe_x=xin;
		pipe_stream_x[0]=xin;
		slow_stage0 = 0;
		slow_stage1 = 0;
		slow_stage2 = 0;
		slow_stage3 = 0;
		
	}
	
	var mispec : boolean;
		

	
	procedure next_pipe() modifies 
		cs,cpt,pipe_i,pipe_x,
		slow_stage0,slow_stage1,slow_stage2,slow_stage3,
		pipe_stream_x; 
	{
		slow_stage3 = slow_stage2;
		slow_stage2 = slow_stage1;
		slow_stage1 = slow_stage0;
		slow_stage0 = slow(pipe_x);
		case 
			cs==PROCEED: {
				if (!cond(pipe_x)) {
					pipe_i=(pipe_i+1); 
					pipe_x = fast(pipe_x);				
					pipe_stream_x[pipe_i]=pipe_x;
				} else {
					cs=STALL;
					cpt=N;
				}
			}
			cs==STALL:  {
				if (cpt==0) {
					pipe_i=(pipe_i+1); 
					pipe_x = slow(pipe_x);				
					pipe_stream_x[pipe_i]=pipe_x;
					
					cs=PROCEED;
				} else {
					cpt=cpt-1;
				}
			}
		esac
	}



	var seed,t : integer ;
	
	init {
		havoc seed;
		t=0;
		cs=PROCEED;
		mispec=false;
		call init_pipe(seed);
		call init_seq(seed);
	}

	next {
		t'=t+1;		
		call next_seq();
		call next_pipe();
	}

	invariant inv_t : (t>=0);
	
	invariant inv_dynamic : (cs!=ROLLBACK );

	
	invariant inv_shift_reg_slow4: (t>=4) ==> (slow_stage3==slow(history(pipe_x,4)));



	//********** PASSED

	/* Not too sisure why this is needed but profof fails otherwise */
	invariant inv_pipe_array2bis : (t>=0) ==> ((seq_i>=0) && (pipe_i>=0));

	/* The folloging invariant proves that seq_stream_x[i] comtains the iˆth iteration */
	invariant inv_seq_array_all : (forall (k:integer) :: 
			(k>0 && k<=seq_i) ==> (seq_stream_x[k]==update_x(seq_stream_x[k-1])));
	
	/* Prove dynamic system liveness by showing weak monotony for pipe_i */
	invariant alwaysproceed: ((t>4)==>(history(pipe_i,5)<pipe_i));

	/* The folloging invariant proves that pipe_stream_x[i] comtains the iˆth iteration */
	invariant inv_pipe_array_all : (forall (k:integer) :: 
			(k>0 && k<=pipe_i) ==> (pipe_stream_x[k]==update_x(pipe_stream_x[k-1])));

	// This is the target invariant which provers that the two traces are equal
	invariant inv_arrayequ_all : (forall (k:integer) :: 
			(k>0 && k<=pipe_i && k<=seq_i) ==> (pipe_stream_x[k-1]==seq_stream_x[k-1]));


	
	
	control {
		// Induction depths
		vobj = induction(6); // 2*N;
		check;
		print_results;
// 		vobj.print_cex(t,cs,pipe_i,mispec,pipe_x,slow_stage3seq_x,seq_i,pipe_stream_x,seq_stream_x);
 		vobj.print_cex(t,cs,pipe_x,slow_stage0,slow_stage1,slow_stage2,slow_stage3);
	}

	
}
